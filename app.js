require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const app = express();

app.use(morgan("dev"));
app.use(express.json());

const { PORT } = process.env;

// basic routes : secondsport.com/api/v1/
app.get("/api/v1", (req, res) => {
  res.status(200).json({
    status: true,
    message: "welcome to API SecondSport",
    data: null,
  });
});

app.listen(PORT, () => {
  console.log("server running on port ", PORT);
});
